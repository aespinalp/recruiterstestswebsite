---
layout: page
title: Contexte
permalink: /contexte/
---

### Quelques éléments de contexte

DSI Group s'est lancé dans une expérimentation ambitieuse dont l'objectif est d'**éprouver des méthodes d'analyse automatique des offres d'emploi et des Cvs des candidats** dans les processus d'embauche.

En ce qui concerne les offres d'emploi, l'analyse automatique a pour finalité **l'identification des besoins les plus essentiels** (compétences professionnelles ou conditions spécifiques du poste) qui sont exprimés sur le texte de chaque offre. 

Un besoin essentiel pour l'embauche correspond à un prérequis exprimé dans l'offre d'emploi que le candidat idéal doit satisfaire. Dans ce sens, nous ne pouvons pas embaucher un candidat qui manque d'un besoin essentiel. 

Nous vous sollicitons aujourd'hui pour cette expérimentation en faisant appel aux recruteurs, pour l'identification des besoins les plus essentiels sur des offres que vous avez traité précédemment.

L'annotation de ces besoins nous permettra d'identifier quels sont les indicateurs du texte de l'offre qui sont associés à votre perception sur ce qui est le plus pertinent pour l'embauche. 

Grâce à votre participation, nous pourrons ainsi répondre aux questions suivantes : _quels sont les indicateurs du texte de l'offre d'emploi qui permettent d'identifier automatiquement les besoins les plus essentiels pour l'embauche ?_ _Quels sont les indicateurs du texte qui permettent d'identifier automatiquement les parties du CV des candidats qui permettent de mettre en évidence qu'ils satisfassent les besoins essentiels de l'offre d'emploi ?_ _Comment cette analyse automatique impacte la qualité d'un processus de correspondance automatique entre des profils de candidats et des offres d'emploi?_ _Quel est l'impact réel de cette analyse sur le contexte quotidien du recruteur?_ (_En termes de réduction du temps investi sur la phase de screening de CVs_)

Notre corpus d'expérimentation est constitué des CVs des candidats anonymisés et des offres d'emploi traitées par chaque recruteur de DSI Group sur Boondmanager.
 
Après la participation de chaque recruteur, plusieurs modèles d'intelligence artificielle seront testés et comparés afin de caractériser la relation concrète entre les indicateurs du texte de l'offre d'emploi et du CV par rapport à la perception du recruteur sur ce qui est essentiel dans chaque processus d'embauche. Nous comparons également les résultats automatiques avec l'analyse humaine, plus fine et plus experte, afin 1) de valider ou invalider les modèles IA, et 2) de les améliorer. La "validation humaine" est ainsi cruciale pour mener à bien notre expérimentation. Celle-ci s'appuie sur des offres d'emploi choisies au hasard, et sur une série de tests de validation associés.

### Démarrer l'évaluation

Nous vous invitons à explorer le contenu de chaque test proposé dans cette expérimentation : 

[<button class="button">Section suivante : Le contenu de chaque test</button>](/recruiterstestswebsite/test1/)


