---
layout: home
---

> Quels sont les indicateurs du texte de l'offre d'emploi qui permettent d'identifier automatiquement les besoins exprimés les plus essentiels ?
>
> Quels sont les indicateurs du texte utiles pour identifier automatiquement les parties du CV des candidats qui permettent de mettre en évidence qu'ils satisfassent les besoins exprimés sur l'offre d'emploi ?
>
> Comment cette analyse automatique impacte la qualité d'un processus de correspondance automatique entre des CVs de candidats et des offres d'emploi ?
>
> Quel est l'impact réel de cette analyse sur le contexte quotidien du recruteur? (_En termes de réduction du temps investi sur la phase de screening de CVs_)

Ces sont les questions auxquelles cette expérimentation tente de répondre.

Pour y répondre, nous avons besoin de votre expertise de recruteur.

Suivez le guide :

[<button class="large button">Quelques éléments de contexte</button>]({{ site.baseurl }}/contexte/)

[<button class="large button">Explication des tests</button>]({{ site.baseurl }}/test1/)

[<button class="large button">Liens vers les tests proposés</button>]({{ site.baseurl }}/tests_links/)

[<button class="large button disabled">Tests avancés</button>]({{ site.baseurl }}/) (Test encore non disponible.)
