---
layout: page
title: Les tests proposés
permalink: /test1/
---

## Explication de chaque test

Trois tests vous sont proposés pour un ensemble de processus d'embauche que vous avez suivi sur Boondmanager. 

### Test 1

Pour ce test, nous avons préparé un ensemble d'offres d'emploi que vous avez traité, et nous vous proposons de 
surligner les besoins les plus essentiels et indispensables pour l'embauche qui sont exprimés dans chaque offre. 
Ce surlignage ou annotation se déroule dans un document Word. Ce document Word contient le texte de l'offre d'emploi à surligner. 

**Remarque :** toute offre d'emploi contient une annotation en bleu
qui a été réalisée par une intelligence artificielle qui a éssayé d'identifier par elle-même les besoins les plus essentiels. 

Ci-dessous, les instructions pour surligner chaque document : 

1 - À l'aide de votre souris, sélectionnez un besoin essentiel sur l'offre d'emploi :

![Sélectionnez un besoin essentiel sur l'offre d'emploi]({{ site.baseurl }}/assets/illustr/test_1_p1.png)

2 - Utilisez le bouton "Couleur de Surlignage" pour le surligner :  

![Utilisez le bouton Couleur de Surlignage]({{ site.baseurl }}/assets/illustr/test_1_p2.jpg)

3 - Si vous avez des remarques de n'importe quel type à réaliser, vous pouvez les spécifier dans la section **Remarques** ou ajoutez les comme des commentaires au document.

![Remarques]({{ site.baseurl }}/assets/illustr/test_2_p_2.png)


Veuiller réaliser le même processus pour chaque besoin essentiel que vous identifierez dans le texte de l'offre. Une fois vous aurez surligné les offres qui vous sont proposées, nous vous prions de le communiquer au mail sing [albeiro.espinal@dsi-globalservices.fr](mailto:albeiro.espinal@dsi-globalservices.fr)    

### Test 2

Le deuxième test proposé dépend directement du premier test et il sera activé une fois vous nous aurez communiqué sur la finalisation du premier test. Il consiste à fournir une estimation du niveau de pertinence (entre 0 et 10) pour chaque besoin
que vous avez choisi lors du Test 1, et de justifier pourquoi avous avez fait ce choix. Nous pourrons vous proposer d'évaluer d'autres besoins
que vous n'avez pas surligné mais qui ont été détectés par l'intelligence artificielle comme étant essentiels.

Ci-dessous, les instructions pour ce test :

1. La colonne "Besoin de l'offre d'emploi" montre le besoin que nous vous proposons de réaliser.
2. La colonne "Choisi par le recruteur?" indique s'il s'agit d'un besoin que vous avez choisi lors du Test 1.
3. Dans la colonne "Niveau estimé de pertinence", veuillez fournir votre estimation (entre 0 et 10, étant 10 le niveau de pertinence le plus important) sur le niveau de pertinence du besoin respectif.
4. Dans la colonne "Justification", veuillez justifier votre estimation.

![Sélectionnez un besoin essentiel sur l'offre d'emploi]({{ site.baseurl }}/assets/illustr/test_2_p_1.png)


### Test 3

Ce troisième test consiste à annoter les parties du CV qui permettent de mettre en évidence la pertinence d'un candidat par rapport aux besoins les plus essentiels de l'offre d'emploi. Le CV du candidat
qu'on vous propose a été extrait de la plateforme Boondmanager et il a été considéré comme acceptable pour être embauché (En effet, le candidat se trouve
en état "CV Envoyé" "Pressenti pour un projet" ou "Embauché" sur Boondmanager).

Veuiller surligner les parties du texte du CV qui mettent en évidence que le candidat satisfait les besoins que vous avez surligné lors du Test 1 sur l'offre d'emploi. Vous pouvez ajouter
des remarques en bas de page ou des commentaires au document pour fournir des informations supplémentaires. 

Pour réaliser ce test, veuillez suivre les mêmes instructions d'annotation illustrées pour le Test 1

Ci-dessous, les instructions pour surligner chaque CV : 

1 - À l'aide de votre souris, sélectionnez une partie du texte du CV qui met en évidence que le candidat satisfait un ou plusieurs besoins essentiels de l'offre d'emploi :

![Sélectionnez un besoin essentiel sur l'offre d'emploi]({{ site.baseurl }}/assets/illustr/test_3_p_1.png)

2 - Utilisez le bouton "Couleur de Surlignage" pour le surligner :  

![Utilisez le bouton Couleur de Surlignage]({{ site.baseurl }}/assets/illustr/test_3_p_2.jpg)

3 - Veuillez indiquer dans un commentaire à quel besoin essentiel de l'offre d'emploi se trouve associé cette partie du texte du CV du candidat. Pour le faire : 

3.1 - Sélectionnez la partie du texte concernée et cliquez sur le bouton "Ajouter un commentaire" : 

![Ajouter commentaire 1]({{ site.baseurl }}/assets/illustr/test_3_p_3.jpg)

3.2 - Rédigez votre commentaire et cliquez sur "Commenter" pour sauvegarder : 

![Ajouter commentaire 2]({{ site.baseurl }}/assets/illustr/test_3_p_4.png)


4 - Si vous avez des remarques supplémentaire de n'importe quel type à réaliser, vous pouvez les spécifier dans la section **Remarques** ou ajoutez-les comme des commentaires au document.

![Remarques]({{ site.baseurl }}/assets/illustr/test_2_p_2.png)


[<button class="button">Liens vers les tests</button>](/recruiterstestswebsite/tests_links)
