---
layout: page
title: Les tests proposés
permalink: /tests_links/
---

## Les tests proposés à chaque recruteur

**Remarque**. Veuillez vous assurer d'avoir lu les instructions avant de réaliser les tests ([Lien](/recruiterstestswebsite/test1/))

|Recruteur|Test 1|Test 2|Test 3|
|:--|:--|:--|:--|
|**Tests pour Sandrine**   |  | | |
|  *Offre 1* | [<button class="button">Accéder</button>](https://docs.google.com/document/d/19bO9sUYhhmUv0CErwiJ0tFfw2YPSguVS/edit?usp=sharing&ouid=105817691478363864840&rtpof=true&sd=true) | [<button class="button disabled">Accéder</button>](/recruiterstestswebsite/test_contenu) | [<button class="button disabled">Accéder</button>](/recruiterstestswebsite/test_contenu) |
|:--|:--|:--|:--|
| |  | | |
|Tests pour Bassem   |  | | |
|:--|:--|:--|:--|
|Tests pour Priscilla   |  | | |
|:--|:--|:--|:--|
|Tests pour Aurore   |  | | |

[<button class="button">Retour au menu principal</button>](/recruiterstestswebsite/)
